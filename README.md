# Backend challenge

## The goals
Develop an api that can safely and reliably perform the following features.

* signup
* login
* list users
* add portfolio
* delete portfolio
* list portfolio
* portfolio balance
* add trade
* delete trade
* list trades

## Logic
The user has one or more portfolios, each portfolio has a list of trades, and the sum of all trades gives you the portfolio position.

Hint: A trade that _buys_ the asset _cash_, is a positive cash flow into the portfolio.

## Model examples

### User model

```json
{
  "id": "aa43d7ed-a03b-478d-a061-7411ddca000f",
  "created": "2019-08-26T01:40:01.350529+00:00",
  "modified": "2019-08-29T05:58:29.022719+00:00",
  "username": "my_username",
  "password": "xSubEpwq3r0KGpXfoq05ylY6dDfT/HgBUrqL0JMsXy4=",
  "name": "my_first_name my_last_name",
  "email": "my_email@email.com",
  "salt": null,
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2",
  "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8v",
  "token_expiration": "2019-09-10T20:25:55Z",
  "refresh_token_expiration": "2019-09-11T20:25:56Z"
}
```

### Trade model

```json
{
  "id": "aa43d7ed-a03b-478d-a061-7411ddca000f",
  "created": "2019-08-26T01:40:01.350529+00:00",
  "modified": "2019-08-29T05:58:29.022719+00:00",
  "user_executor": "aa43d7ed-a03b-478d-a061-7411ddca000f",
  "portfolio": "aa43d7ed-a03b-478d-a061-7411ddca000f",
  "date": "2019-08-25",
  "number_of_shares": 15,
  "price": 15.48,
  "currency": "USD",
  "market_value": 232.20,
  "action": "buy",
  "notes": null,
  "asset": "PT10Y"
}
```

## The tools

You can use whatever tools you feel will provide a good solution for your problem but restricted to the use of .NET Core 3.1 or .NET 6 and C#.  

## The means  

Consider that both API and database engine run locally on your machine or on a docker container.  

## The expected results

You are expected to deliver an API with swagger and database scripts (or migrations), where the required features work, are secure, reliable and scalable.  
All the source should be shared on a git server repository.  
This challenge is meant for you to bring your 'A-game' and show us what you know, so we take into account every extra you put in.

**If you have any questions or you can't figure out a part for process, please reach out, we are here to work as a team.**

## Final comments

This challenge is meant to be done during 1 week and should take no longer than 20-25h of your time. Take your time to architect, develop and even refactor your code.  
As you will deliver your source in a Git repository, take advantage of that to show us your thought process. _Commit often!_

Once you have your project finished and you are confortable, please reach out so we can send you the instructions in how to procede to submit your work for review.